/*
Теоретичні питання
1. Опишіть своїми словами що таке Document Object Model (DOM)
2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
4. Яка різниця між nodeList та HTMLCollection?

Практичні завдання
 1. Знайдіть всі елементи з класом "feature", запишіть в змінну, вивести в консоль.
 Використайте 2 способи для пошуку елементів.
 Задайте кожному елементу з класом "feature" вирівнювання тексту по - центру(text-align: center).

 2. Змініть текст усіх елементів h2 на "Awesome feature".

 3. Знайдіть всі елементи з класом "feature-title" та додайте в кінець тексту елементу знак оклику "!".
 */

 //ТЗ 1
 const allFeature1 = document.querySelectorAll('.feature');
 console.log(allFeature1);

 const  allFeature2 = document.getElementsByClassName( 'feature');
 console.log(allFeature2);

//ТЗ 2
const h2Elements = document.querySelectorAll('h2');
h2Elements.forEach((element) => {
    element.textContent = 'Awesome feature';
});

const featureTitleElements = document.querySelectorAll('.feature-title');
featureTitleElements.forEach((element) => {
    element.textContent += '!';
});
